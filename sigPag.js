let arrayMisproductos = JSON.parse(localStorage.getItem('productos'));

let producto = {
    descripcion: '',
    imagen: '',
}


let template = `<table><thead><th>Descripción</th><th>Imagen</th></thead><tbody>`;
for (let i = 0; i < arrayMisproductos.length; i++) {
    producto = arrayMisproductos[i];
    template += `<tr><td>${producto.descripcion}</td><td><img src="${producto.imagen}"></td></tr>`;
}
template += "</table></body>";
document.querySelector('.tabla').innerHTML = template;


