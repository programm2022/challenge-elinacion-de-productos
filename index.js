let arrayMisproductos = [];
let producto = {
    descripcion: "",
    imagen: ""
}
if (localStorage.getItem('productos')) {
    arrayMisproductos = JSON.parse(localStorage.getItem('productos'));
} else {
    producto = {
        descripcion: "Parlante Portatil Melon Bluetooth 8 Pulgadas Luces Led​",
        imagen: "https://http2.mlstatic.com/D_NQ_NP_2X_618548-MLA44866133508_022021-F.webp"
    }
    arrayMisproductos.push(producto);

    producto = {
        descripcion: "Microsoft Xbox Series S 512GB Standard color blanco",
        imagen: "https://http2.mlstatic.com/D_NQ_NP_2X_627149-MLA44484230438_012021-F.webp"
    }
    arrayMisproductos.push(producto);
}
document.querySelector("#productos").innerHTML = armarTemplate();

function agregar() {
    let des = document.querySelector("#desc").value.trim();
    let img = document.querySelector("#dmgI").value.trim();
    if (des.length === 0 || img.length === 0) return;
    producto = {
        descripcion: des,
        imagen: img
    }

    arrayMisproductos.push(producto);
    console.log(arrayMisproductos);
    document.querySelector("#productos").innerHTML = armarTemplate();

}
function armarTemplate() {
    let template = '';
    for (let i = 0; i < arrayMisproductos.length; i++) {
        template += `<article>
        <div class="trash" onclick="eliminarItem(${i})"><img src="trash-10-xxl.png"></div>
                    <h2 class="descripcion">${arrayMisproductos[i].descripcion}</h2>
                    <img src="${arrayMisproductos[i].imagen}" class="imagen" >
                     </article>`
    }
    return template;
}

function listado() {
    if(arrayMisproductos.length>0)localStorage.setItem('productos', JSON.stringify(arrayMisproductos));
    location.href = 'sigPag.html';
}

function eliminarItem(numPro) {
    arrayMisproductos.splice(numPro,1);
    
    if(arrayMisproductos.length ===0)localStorage.removeItem('productos');
    // location.href = 'index.html';
    document.querySelector("#productos").innerHTML = armarTemplate();
}
